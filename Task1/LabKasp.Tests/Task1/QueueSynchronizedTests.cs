﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using LabKasp.Task1;
using NUnit.Framework;

namespace LabKasp.Tests.Task1
{
	/// <summary>
	/// Тесты для <see cref="QueueSynchronized{T}"/>
	/// </summary>
	[TestFixture]
	public class QueueSynchronizedTests
	{
		/// <summary>
		/// При одинаковом количестве операций вставки и чтения кол-во элементов - очередь должна остаться пустой
		/// </summary>
		[TestCaseSource(typeof(DataFactoryClass), nameof(DataFactoryClass.MakeOperationTestCases))]
		public void QueueSynchronized_RandomThreadPushAndPop_CountShouldBeZero(Operation[] operations)
		{
			var queue = new QueueSynchronized<int>();
			var threads = new List<Thread>();

			var item = 1;
			foreach (var operation in operations)
			{
				if (operation.IsPop)
					for (var i = 0; i < operation.AmountCalls; i++)
						threads.Add(new Thread(() => queue.Pop()));
				else
					for (var i = 0; i < operation.AmountCalls; i++)
						threads.Add(new Thread(() => queue.Push(item)));
			}

			Parallel.ForEach(threads, t =>
			{
				t.Start();
				t.Join();
			});

			queue.Count.Should().Be(0);
		}

		/// <summary>
		/// При одинаковом количестве операций вставки и чтения кол-во элементов - очередь должна остаться пустой
		/// </summary>
		[TestCaseSource(typeof(DataFactoryClass), nameof(DataFactoryClass.MakeOperationTestCases))]
		public void QueueSynchronized_RandomTaskPushAndPop_CountShouldBeZero(Operation[] operations)
		{
			var queue = new QueueSynchronized<int>();
			var tasks = new List<Task>();

			var item = 1;
			foreach (var operation in operations)
			{
				if (operation.IsPop)
					for (var i = 0; i < operation.AmountCalls; i++)
						tasks.Add(new Task(() => queue.Pop()));
				else
					for (var i = 0; i < operation.AmountCalls; i++)
						tasks.Add(new Task(() => queue.Push(item++)));
			}

			Parallel.ForEach(tasks, t => t.Start());

			Task.WaitAll(tasks.ToArray());

			queue.Count.Should().Be(0);
		}

		/// <summary>
		/// Тестовые данные
		/// </summary>
		private class DataFactoryClass
		{
			/// <summary>
			/// Тестовые данные для проверки <see cref="QueueSynchronized{T}"/>
			/// </summary>
			public static IEnumerable MakeOperationTestCases
			{
				get
				{
					yield return new TestCaseData(new[] { Pop(1), Push(1) });
					yield return new TestCaseData(new[] { Pop(10), Push(10) });
					yield return new TestCaseData(new[] { Pop(10), Push(10), Pop(10), Push(10), Pop(10), Push(10) });
				}
			}

			private static Operation Pop(int amountCalls)
			{
				return new Operation(true, amountCalls);
			}

			private static Operation Push(int amountCalls)
			{
				return new Operation(false, amountCalls);
			}
		}

		public struct Operation
		{
			public Operation(bool isPop, int amountCalls)
			{
				IsPop = isPop;
				AmountCalls = amountCalls;
			}

			public bool IsPop { get; }

			public int AmountCalls { get; }
		}
	}
}
