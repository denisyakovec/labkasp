﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LabKasp.Task1;

namespace LabKasp
{
	class Program
	{
		private static int _popId;

		static void Main(string[] args)
		{
			/*
             1.Надо сделать очередь с операциями push(T) и T pop(). Операции должны поддерживать
обращение с разных потоков. Операция push всегда вставляет и выходит. Операция pop ждет пока
не появится новый элемент. В качестве контейнера внутри можно использовать только
стандартную очередь (Queue) .
             */
			Console.Out.WriteLine("Надо сделать очередь с операциями push(T) и T pop(). Операции должны поддерживать обращение с разных потоков.Операция push всегда вставляет и выходит.Операция pop ждет пока не появится новый элемент.В качестве контейнера внутри можно использовать только стандартную очередь(Queue)");

			Console.Out.WriteLine("\nСколько выполнить операций вставки (введите значение и нажмите Enter)?");
			var readLine = Console.In.ReadLine();

			if (int.TryParse(readLine, out var amount))
			{
				var queue = new QueueSynchronized<int>();

				var threads = new List<Thread>();

				for (var i = 0; i < amount; i++)
				{
					var item = i;
					threads.Add(new Thread(() => Push(queue, item)));
					threads.Add(new Thread(() => Pop(queue)));
				}

				Parallel.ForEach(threads, t =>
				{
					t.Start();
					t.Join();
				});

				Console.Out.WriteLine($"\nКол-во элементов в очереди после всех операций = {queue.Count}");
			}

			Console.Out.WriteLine("\nДля завершения работы нажмите Enter...");
			Console.In.ReadLine();
		}

		private static void Pop<T>(QueueSynchronized<T> queue)
		{
			Console.Out.WriteLine($"Pop{Thread.CurrentThread.ManagedThreadId}() started...");

			var item = queue.Pop();

			Console.Out.WriteLine($"Pop{Thread.CurrentThread.ManagedThreadId}() finished. Item={item}.");
		}

		private static void Push<T>(QueueSynchronized<T> queue, T item)
		{
			Console.Out.WriteLine($"\tPush{Thread.CurrentThread.ManagedThreadId}({item}) started...");

			queue.Push(item);

			Console.Out.WriteLine($"\tPush{Thread.CurrentThread.ManagedThreadId}({item}) finished.");
		}
	}
}
