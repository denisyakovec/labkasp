﻿using System.Collections.Generic;
using System.Threading;

namespace LabKasp.Task1
{
	/// <summary>
	/// Представляет собой очередь с синхронизированным доступом.
	/// Операции поддерживают обращение с разных потоков.
	/// </summary>
	public class QueueSynchronized<T>
	{
		private readonly Queue<T> _queue;
		private readonly ManualResetEventSlim _pushEvent;

		private readonly object _lockPop;

		/// <summary>
		/// Количество элементов в очереди
		/// </summary>
		public int Count => _queue.Count;

		public QueueSynchronized()
		{
			_lockPop = new object();
			_queue = new Queue<T>();
			_pushEvent = new ManualResetEventSlim(false);
		}

		/// <summary>
		/// Извлечь элемент из очереди.
		/// Операция pop ждет пока не появится новый элемент.
		/// </summary>
		/// <returns></returns>
		public T Pop()
		{
			lock (_lockPop)
			{
				_pushEvent.Wait();

				var item = _queue.Dequeue();

				if (_queue.Count == 0)
					_pushEvent.Reset();

				return item;
			}
		}

		/// <summary>
		/// Поместить элемент в очередь.
		/// Операция push всегда вставляет и выходит.
		/// </summary>
		/// <param name="item"></param>
		public void Push(T item)
		{
			_queue.Enqueue(item);

			_pushEvent.Set();
		}
	}
}
