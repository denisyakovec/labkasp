﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using LabKasp.Task2;
using NUnit.Framework;

namespace LabKasp.Tests.Task2
{
	/// <summary>
	/// Тесты для <see cref="NumberHelper"/>
	/// </summary>
	[TestFixture]
	public class NumberHelperTests
	{
		[Test]
		public void MakeNumberPairs_NumbersEmpty_ReturnsEmpty()
		{
			var numbers = Array.Empty<int>();

			var extractedNumberPairs = NumberHelper.MakeNumberPairs(numbers);

			extractedNumberPairs.Should().BeEmpty();
		}

		[Test]
		public void MakeNumberPairs_NumbersHasOne_ReturnsEmpty()
		{
			var numbers = new[] { 1 };

			var extractedNumberPairs = NumberHelper.MakeNumberPairs(numbers);

			extractedNumberPairs.Should().BeEmpty();
		}

		[Test]
		public void MakeNumberPairs_NumbersHasPairs_ValidEachPair()
		{
			var numbers = new[] { -1, 2, 0 };

			var extractedNumberPairs = NumberHelper.MakeNumberPairs(numbers).ToArray();

			extractedNumberPairs.Length.Should().Be(3);
			extractedNumberPairs.Any(p => p.Number1 == -1 && p.Number2 == 2).Should().BeTrue();
			extractedNumberPairs.Any(p => p.Number1 == 2 && p.Number2 == 0).Should().BeTrue();
			extractedNumberPairs.Any(p => p.Number1 == -1 && p.Number2 == 0).Should().BeTrue();
		}

		[TestCaseSource(typeof(DataFactoryClass), nameof(DataFactoryClass.MakeNumberPairsTestCases))]
		public int MakeNumberPairs_NumbersHasMoreOne_ReturnsPairs(int[] numbers)
		{
			var extractedNumberPairs = NumberHelper.MakeNumberPairs(numbers);

			return extractedNumberPairs.Count();
		}

		[Test]
		public void GetNumberPairsBySum_NumbersEmpty_ReturnsEmpty()
		{
			var numbers = Array.Empty<int>();
			const int sumResult = 0;

			var extractedNumberPairs = NumberHelper.GetNumberPairsBySum(numbers, sumResult);

			extractedNumberPairs.Should().BeEmpty();
		}

		[Test]
		public void GetNumberPairsBySum_NumbersNull_ReturnsEmpty()
		{
			int[] numbers = null;
			const int sumResult = 0;

			var extractedNumberPairs = NumberHelper.GetNumberPairsBySum(numbers, sumResult);

			extractedNumberPairs.Should().BeEmpty();
		}

		[Test]
		public void GetNumberPairsBySum_NumbersHasOneNumber_ReturnsEmpty()
		{
			var numbers = new List<int> { 1 };
			const int sumResult = 0;

			var extractedNumberPairs = NumberHelper.GetNumberPairsBySum(numbers, sumResult);

			extractedNumberPairs.Should().BeEmpty();
		}

		[Test]
		public void GetNumberPairsBySum_NumbersHasNotValidPair_ReturnsEmpty()
		{
			var numbers = new List<int> { -2, 0, 1, 2, 3, 4 };
			const int sumResult = 33;

			var extractedNumberPairs = NumberHelper.GetNumberPairsBySum(numbers, sumResult);

			extractedNumberPairs.Should().BeEmpty();
		}

		[Test]
		public void GetNumberPairsBySum_NumbersHasPairs_ValidEachPair()
		{
			var numbers = new[] { -1, 2, 0, 1, -4, 3 };
			const int sumResult = 1;

			var extractedNumberPairs = NumberHelper.GetNumberPairsBySum(numbers, sumResult).ToArray();

			extractedNumberPairs.Length.Should().Be(2);
			extractedNumberPairs.Any(p => p.Number1 == -1 && p.Number2 == 2).Should().BeTrue();
			extractedNumberPairs.Any(p => p.Number1 == 0 && p.Number2 == 1).Should().BeTrue();
		}

		[TestCaseSource(typeof(DataFactoryClass), nameof(DataFactoryClass.GetNumberPairsBySumTestCases))]
		public int GetNumberPairsBySum_NumbersHasPairsBySum_ReturnsPairs(int[] numbers, int sumResult)
		{
			var extractedNumberPairs = NumberHelper.GetNumberPairsBySum(numbers, sumResult).ToArray();

			return extractedNumberPairs.Length;
		}

		/// <summary>
		/// Тестовые данные
		/// </summary>
		private class DataFactoryClass
		{
			/// <summary>
			/// Тестовые данные для проверки <see cref="NumberHelper.MakeNumberPairs"/>
			/// </summary>
			public static IEnumerable MakeNumberPairsTestCases
			{
				get
				{
					yield return new TestCaseData(new[] { 1, 2 }).Returns(1);
					yield return new TestCaseData(new[] { -1, -1, 3 }).Returns(3);
					yield return new TestCaseData(new[] { -1, 1, 3, 5 }).Returns(6);
					yield return new TestCaseData(new[] { 1, 2, 3, 0, 3 }).Returns(10);
				}
			}

			/// <summary>
			/// Тестовые данные для проверки <see cref="NumberHelper.GetNumberPairsBySum"/>
			/// </summary>
			public static IEnumerable GetNumberPairsBySumTestCases
			{
				get
				{
					yield return new TestCaseData(new[] { 1, 2, 0, 0 }, 3).Returns(1);
					yield return new TestCaseData(new[] { -1, -1, 3 }, -2).Returns(1);
					yield return new TestCaseData(new[] { 3, 2, 1, 0, 3 }, 3).Returns(3);
				}
			}
		}
	}
}
