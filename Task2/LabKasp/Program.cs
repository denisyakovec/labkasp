﻿using System;
using System.Linq;
using LabKasp.Task2;

namespace LabKasp
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.Out.WriteLine("Есть коллекция чисел и отдельное число Х.\nНадо вывести все пары чисел, которые в сумме равны заданному Х.");

			Console.Out.WriteLine("\nВведите список целых чисел через пробел и нажмите Enter:");
			var inputNumbers = Console.In.ReadLine();

			var numbers = ParseNumbers(inputNumbers);

			Console.Out.WriteLine("\nВведите значение Х и нажмите Enter:");
			var inputX = Console.In.ReadLine();

			var x = ParseX(inputX);

			DisplayNumberPairsBySum(numbers, x);

			Console.Out.WriteLine("\nДля завершения работы нажмите Enter...");
			Console.In.ReadLine();
		}

		private static int ParseX(string inputX)
		{
			int x;

			if (!string.IsNullOrWhiteSpace(inputX))
				int.TryParse(inputX, out x);
			else
				x = 0;

			return x;
		}

		private static int[] ParseNumbers(string numbersBySpace)
		{
			int[] numbers;

			if (!string.IsNullOrWhiteSpace(numbersBySpace))
				numbers = numbersBySpace.Split(' ').Select(s =>
				{
					int number;
					int.TryParse(s, out number);
					return number;
				}).ToArray();
			else
				numbers = Array.Empty<int>();

			return numbers;
		}

		private static void DisplayNumberPairsBySum(int[] numbers, int sumResult)
		{
			var separatorHeader = new string('*', 60);

			Console.Out.WriteLine($"\n{separatorHeader}");
			Console.Out.WriteLine("Вывод результатов");
			Console.Out.WriteLine(separatorHeader);

			var separatorLine = new string('-', 40);

			Console.Out.WriteLine("\nИсходная коллекция чисел:");
			Console.Out.WriteLine(separatorLine);
			Console.Out.WriteLine(string.Join(" ", numbers));
			Console.Out.WriteLine("\nИсходное число Х:");
			Console.Out.WriteLine(separatorLine);
			Console.Out.WriteLine(sumResult);

			var pairs = NumberHelper.GetNumberPairsBySum(numbers, sumResult).ToArray();

			Console.Out.WriteLine("\nРезультат");
			Console.Out.WriteLine(separatorLine);
			if (pairs.Any())
			{
				Console.Out.WriteLine("\nПары чисел, сумма которых равна значению Х:");
				foreach (var pair in pairs)
					Console.Out.WriteLine($"{pair.Number1}; {pair.Number2}");
			}
			else
				Console.Out.WriteLine("\nПар чисел, удовлетворяющих критерию, не найдено.");
		}
	}
}
