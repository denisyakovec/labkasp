﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LabKasp.Task2
{
	/// <summary>
	/// Вспомогательный класс для обработки чисел
	/// </summary>
	public static class NumberHelper
	{
		/// <summary>
		/// Извлечь все пары чисел, которые в сумме равны заданному <paramref name="sumResult"/>
		/// </summary>
		/// <param name="numbers">Коллекция чисел</param>
		/// <param name="sumResult">Отдельное число Х</param>
		/// <returns>Список пар чисел <see cref="NumberPair"/> если есть совпадения, иначе null</returns>
		public static IEnumerable<NumberPair> GetNumberPairsBySum(IEnumerable<int> numbers, int sumResult)
		{
			if (numbers == null)
				return Array.Empty<NumberPair>();

			var resultList = MakeNumberPairs(numbers)
				.Where(pair => pair.Number1 + pair.Number2 == sumResult)
				.ToArray();

			return resultList;
		}

		/// <summary>
		/// Создать список числовых пар из списка чисел
		/// </summary>
		/// <param name="numbers">Список чисел</param>
		/// <returns></returns>
		public static IEnumerable<NumberPair> MakeNumberPairs(IEnumerable<int> numbers)
		{
			var listNumbers = numbers.ToArray();

			var numberPairs = listNumbers
				.Select((number, index) => new { number, index })
				.SelectMany(x => listNumbers.Skip(x.index + 1), (x, y) => new NumberPair(x.number, y));

			return numberPairs;
		}
	}
}
