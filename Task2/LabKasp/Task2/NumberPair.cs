﻿namespace LabKasp.Task2
{
	/// <summary>
	/// Описывает два числа
	/// </summary>
	public struct NumberPair
	{
		/// <summary>
		/// Первое число
		/// </summary>
		public int Number1 { get; set; }

		/// <summary>
		/// Второе число
		/// </summary>
		public int Number2 { get; set; }

		/// <summary>
		/// ctor
		/// </summary>
		/// <param name="number1"></param>
		/// <param name="number2"></param>
		public NumberPair(int number1, int number2)
		{
			Number1 = number1;
			Number2 = number2;
		}
	}
}